package com.sedex.connect.company.controller.dto

class ErrorResponse(val message: String)
package com.sedex.connect.company.repository.entity

import javax.persistence.*

@Entity
@Table(name = "company_address")
data class CompanyAddress(val addressLine1: String, val addressLine2: String, val city: String, val state: String,
                          val postalCode: String, val countryCode: String){
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "id")
    private var id: Long? = null
}
package com.sedex.connect.company.controller.dto

data class CompanyList(val companyList: List<CompanyResponse>?)

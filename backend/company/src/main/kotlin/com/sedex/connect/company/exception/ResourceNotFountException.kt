package com.sedex.connect.company.exception

class ResourceNotFountException(reason: String?) : RuntimeException(reason)

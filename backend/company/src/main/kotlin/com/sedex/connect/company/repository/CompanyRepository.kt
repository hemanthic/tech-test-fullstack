package com.sedex.connect.company.repository

import com.sedex.connect.company.repository.entity.Company
import com.sedex.connect.company.repository.entity.CompanyAddress
import org.springframework.data.jpa.repository.JpaRepository
import org.springframework.stereotype.Repository
import java.util.*

/**
 * Scope of improvement: Schema, primary key and index must be created on Database side.
 *
 * @see Company
 * @see CompanyAddress
 */
@Repository
interface CompanyRepository : JpaRepository<Company, UUID>{
    fun findByCompanyName(companyName:String): Company?
}


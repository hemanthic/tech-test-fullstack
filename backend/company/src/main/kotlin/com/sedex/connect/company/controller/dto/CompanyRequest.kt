package com.sedex.connect.company.controller.dto

/**
 * CompanyName is mandatory field.
 *
 * Scope of improvement: Incorporated Date, phone number, email address, postal code and country can be
 * validated again standard formats(@Validated)
 *
 * @see Address
 */
data class CompanyRequest(val companyName: String, val companyType: String?, val natureOfBusiness: String?,
                          val incorporatedDate: String?, val emailAddress: String?, val phoneNumber: String?,
                          val address: Address?)


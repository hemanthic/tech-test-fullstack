package com.sedex.connect.company.service

import com.sedex.connect.company.controller.dto.Address
import com.sedex.connect.company.controller.dto.CompanyRequest
import com.sedex.connect.company.exception.DataAlreadyExistException
import com.sedex.connect.company.exception.ResourceNotFountException
import com.sedex.connect.company.repository.CompanyRepository
import com.sedex.connect.company.repository.entity.Company
import com.sedex.connect.company.repository.entity.CompanyAddress
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.stereotype.Service
import org.springframework.transaction.annotation.Transactional
import java.util.*

@Service
class CompanyService @Autowired constructor(private val companyRepository: CompanyRepository) {

    @Transactional
    fun createCompany(companyRequest: CompanyRequest): Company {

        val companyName = companyRequest.companyName
        val findByCompanyName = companyRepository.findByCompanyName(companyName)
        findByCompanyName?.let {
            val reason = "Company with name is already exist: $companyName"
            logger.error { reason }
            throw DataAlreadyExistException(reason)
        }
        val company = toCompanyEntity(companyRequest)
        val savedCompany = companyRepository.save(company)
        logger.debug { "Saved entity: $savedCompany" }

        return savedCompany
    }

    private fun toCompanyEntity(companyRequest: CompanyRequest): Company {
        return Company(companyRequest.companyName, companyRequest.companyType, companyRequest.natureOfBusiness,
                companyRequest.incorporatedDate, companyRequest.emailAddress, companyRequest.phoneNumber,
                toCompanyAddress(companyRequest.address))
    }

    private fun toCompanyAddress(address: Address?): CompanyAddress? {
        return address?.let {
            CompanyAddress(address.addressLine1, address.addressLine2, address.city, address.state,
                    address.postalCode, address.countryCode)
        }
    }

    @Transactional(readOnly = true)
    fun getCompanyList(): List<Company> {
        // Scope of improvement:Pagination
        return companyRepository.findAll()
    }

    @Transactional(readOnly = true)
    fun getCompany(id: String): Company {
        val findById = companyRepository.findById(UUID.fromString(id))
        return findById.orElseThrow {
            throw ResourceNotFountException("Company is not found with id :$id")
        }
    }

    companion object {
        private val logger = KotlinLogging.logger {}
    }
}

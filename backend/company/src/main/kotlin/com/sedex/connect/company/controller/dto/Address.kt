package com.sedex.connect.company.controller.dto

data class Address(val addressLine1: String, val addressLine2: String, val city: String, val state: String,
                   val postalCode: String, val countryCode: String)
package com.sedex.connect.company.controller.advice

import com.sedex.connect.company.controller.dto.ErrorResponse
import com.sedex.connect.company.exception.DataAlreadyExistException
import com.sedex.connect.company.exception.ResourceNotFountException
import mu.KotlinLogging
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.web.bind.annotation.ControllerAdvice
import org.springframework.web.bind.annotation.ExceptionHandler
import java.lang.IllegalArgumentException

@ControllerAdvice
class ControllerAdvicer {

    @ExceptionHandler(ResourceNotFountException::class)
    fun resourceNotFoundException(e: ResourceNotFountException): ResponseEntity<ErrorResponse> {
        return error(e, HttpStatus.NOT_FOUND)
    }

    @ExceptionHandler(DataAlreadyExistException::class)
    fun resourceAlreadyExistException(e: DataAlreadyExistException): ResponseEntity<ErrorResponse> {
        return error(e, HttpStatus.CONFLICT)
    }

    @ExceptionHandler(IllegalArgumentException::class)
    fun badRequestException(e: IllegalArgumentException): ResponseEntity<ErrorResponse> {
        return error(e, HttpStatus.BAD_REQUEST)
    }

    private fun error(exception: Exception, httpStatus: HttpStatus): ResponseEntity<ErrorResponse> {
        logger.error { exception }
        val message: String = exception.message ?: exception.javaClass.simpleName
        return ResponseEntity(ErrorResponse(message), httpStatus)
    }

    companion object {
        private val logger = KotlinLogging.logger {}
    }
}
package com.sedex.connect.company.controller.dto

data class CompanyResponse(val id: String, val companyName: String, val companyType: String?,
                           val natureOfBusiness: String?, val incorporatedDate: String?, val emailAddress: String?,
                           val phoneNumber: String?, val address: Address?)

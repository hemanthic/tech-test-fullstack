package com.sedex.connect.company.exception

class DataAlreadyExistException(reason: String) : RuntimeException(reason)

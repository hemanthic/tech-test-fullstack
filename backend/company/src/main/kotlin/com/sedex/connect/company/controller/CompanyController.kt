package com.sedex.connect.company.controller

import com.sedex.connect.company.controller.dto.Address
import com.sedex.connect.company.controller.dto.CompanyList
import com.sedex.connect.company.controller.dto.CompanyRequest
import com.sedex.connect.company.controller.dto.CompanyResponse
import com.sedex.connect.company.repository.entity.Company
import com.sedex.connect.company.repository.entity.CompanyAddress
import com.sedex.connect.company.service.CompanyService
import mu.KotlinLogging
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.validation.annotation.Validated
import org.springframework.web.bind.annotation.*

@Validated
@RestController
class CompanyController @Autowired constructor(private val companyService: CompanyService) {

    @PostMapping("/company", consumes = [MediaType.APPLICATION_JSON_VALUE],
            produces = [MediaType.APPLICATION_JSON_VALUE])
    fun createCompany(@RequestBody companyRequest: CompanyRequest): CompanyResponse {
        logger.info { "Request made: $companyRequest" }
        val createCompany = companyService.createCompany(companyRequest)
        val companyResponse = toCompanyResponse(createCompany)
        logger.info { "Response received: $companyResponse" }
        return companyResponse
    }

    @GetMapping("/company", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCompanyList(): CompanyList {
        logger.info { "Company list is requested" }
        val companyList = companyService.getCompanyList()
        val companyListResponse = CompanyList(companyList.map { toCompanyResponse(it) })
        logger.info { "Response received: $companyListResponse" }
        return companyListResponse
    }

    @GetMapping("/company/{id}", produces = [MediaType.APPLICATION_JSON_VALUE])
    fun getCompanyList(@PathVariable("id") id: String): CompanyResponse {
        logger.info { "Search company by id: $id" }
        val company = companyService.getCompany(id)
        val companyResponse = toCompanyResponse(company)
        logger.info { "Response received: $companyResponse" }
        return companyResponse
    }

    private fun toCompanyResponse(company: Company): CompanyResponse {
        return CompanyResponse(company.id.toString(), company.companyName, company.companyType, company.natureOfBusiness,
                company.incorporatedDate, company.emailAddress, company.phoneNumber, toResponseAddress(company.address))
    }

    private fun toResponseAddress(address: CompanyAddress?): Address? {
        return address?.let { Address(address.addressLine1, address.addressLine2, address.city, address.state, address.postalCode, address.countryCode) }
    }

    companion object {
        private val logger = KotlinLogging.logger {}
    }

}

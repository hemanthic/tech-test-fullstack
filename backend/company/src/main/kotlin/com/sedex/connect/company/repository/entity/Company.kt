package com.sedex.connect.company.repository.entity

import org.hibernate.annotations.GenericGenerator
import java.util.*
import javax.persistence.*

@Entity
@Table(name = "company")
data class Company(val companyName: String, val companyType: String? = null, val natureOfBusiness: String? = null,
                   val incorporatedDate: String? = null, val emailAddress: String? = null, val phoneNumber: String? = null,
                   @OneToOne(cascade = [CascadeType.ALL])
                   @JoinColumn(name = "company_address_id", referencedColumnName = "id")
                   val address: CompanyAddress? = null) {

    @Id
    @GeneratedValue(generator = "UUID")
    @GenericGenerator(name = "UUID", strategy = "org.hibernate.id.UUIDGenerator")
    @Column(name = "id", updatable = false, nullable = false)
    var id: UUID? = null

}

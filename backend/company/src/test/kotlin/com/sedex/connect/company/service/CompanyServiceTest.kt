package com.sedex.connect.company.service

import com.sedex.connect.company.controller.dto.Address
import com.sedex.connect.company.controller.dto.CompanyRequest
import com.sedex.connect.company.exception.ResourceNotFountException
import com.sedex.connect.company.repository.CompanyRepository
import com.sedex.connect.company.repository.entity.Company
import com.sedex.connect.company.repository.entity.CompanyAddress
import org.assertj.core.api.Assertions
import org.assertj.core.api.Assertions.*
import org.junit.jupiter.api.BeforeEach
import org.junit.jupiter.api.Test
import org.junit.jupiter.api.extension.ExtendWith
import org.mockito.Mock
import org.mockito.Mockito.`when`
import org.mockito.Mockito.any
import org.mockito.junit.jupiter.MockitoExtension
import java.util.*

@ExtendWith(MockitoExtension::class)
class CompanyServiceTest {

    @Mock
    private lateinit var repository: CompanyRepository

    private lateinit var companyService: CompanyService

    @BeforeEach
    fun setUp() {
        companyService = CompanyService(repository)
    }


    @Test
    fun testCompanyCreate() {
        //When
        val companyAddress = CompanyAddress("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val company = Company("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", companyAddress)
        company.id = UUID.randomUUID()

        //When
        `when`(repository.save(any(Company::class.java))).thenReturn(company)

        val address = Address("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val createCompanyRequest = CompanyRequest("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", address)

        val actualCreatedCompany = companyService.createCompany(createCompanyRequest)

        assertThat(actualCreatedCompany).isEqualTo(company)
    }

    @Test
    fun testCompanyList() {

        //Given
        val companyAddress = CompanyAddress("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val company = Company("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", companyAddress)
        company.id = UUID.randomUUID()

        //When
        `when`(repository.findAll()).thenReturn(listOf(company))

        val actualCompanyList = companyService.getCompanyList()

        assertThat(actualCompanyList).isEqualTo(listOf(company))
    }

    @Test
    fun testGetCompany() {

        //Given
        val randomUUID = UUID.randomUUID()
        val companyAddress = CompanyAddress("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val company = Company("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", companyAddress)
        company.id = randomUUID

        //When
        `when`(repository.findById(randomUUID)).thenReturn(Optional.of(company))

        val actualCompany = companyService.getCompany(randomUUID.toString())

        assertThat(actualCompany).isEqualTo(company)
    }

    @Test
    fun testGetCompanyNonExistId() {

        //Given
        val randomUUID = UUID.randomUUID()

        //When
        `when`(repository.findById(randomUUID)).thenReturn(Optional.empty())

        val id = randomUUID.toString()
        assertThatThrownBy { companyService.getCompany(id) }
                .isInstanceOf(ResourceNotFountException::class.java)
                .hasMessageContaining("Company is not found with id :$id");
    }

}

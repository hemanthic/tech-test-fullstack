package com.sedex.connect.company

import com.sedex.connect.company.controller.dto.CompanyList
import com.sedex.connect.company.controller.dto.CompanyResponse
import com.sedex.connect.company.repository.CompanyRepository
import org.assertj.core.api.Assertions.assertThat
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment.RANDOM_PORT
import org.springframework.boot.test.web.client.TestRestTemplate
import org.springframework.boot.web.server.LocalServerPort
import org.springframework.http.HttpStatus
import java.util.*

@SpringBootTest(webEnvironment = RANDOM_PORT, properties = ["spring.datasource.url=jdbc:h2:mem:testdb;DB_CLOSE_DELAY=-1"])
class SmokeITTest {

    @LocalServerPort
    private var port = -1

    @Autowired
    private lateinit var restTemplate: TestRestTemplate

    @Autowired
    private lateinit var companyRepository: CompanyRepository

    @Test
    fun smokeTestCreateCompany() {
        //Given
        val createCompanyRequest = TestData.testCompanyRequestData()
        val createCompanyResponse = TestData.testCompanyResponseData(UUID.randomUUID().toString())
        companyRepository.deleteAll()

        //When
        val responseEntity = restTemplate.postForEntity("http://localhost:$port/interview/v0/company",
                createCompanyRequest, CompanyResponse::class.java)

        assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(responseEntity.body).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(createCompanyResponse)
    }

    @Test
    fun smokeTestGetCompanyList() {
        //Given
        val createCompanyRequest = TestData.testCompanyRequestData()
        companyRepository.deleteAll()
        restTemplate.postForEntity("http://localhost:$port/interview/v0/company", createCompanyRequest, CompanyResponse::class.java)

        //When
        val responseEntity = restTemplate.getForEntity("http://localhost:$port/interview/v0/company", CompanyList::class.java)

        assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(responseEntity.body?.companyList).hasSize(1)

    }

    @Test
    fun smokeTestGetACompany() {
        //Given
        val createCompanyRequest = TestData.testCompanyRequestData()
        val createCompanyResponse = TestData.testCompanyResponseData(UUID.randomUUID().toString())
        companyRepository.deleteAll()
        val postForEntity = restTemplate.postForEntity("http://localhost:$port/interview/v0/company", createCompanyRequest, CompanyResponse::class.java)

        //When
        val id = postForEntity.body?.id
        val responseEntity = restTemplate.getForEntity("http://localhost:$port/interview/v0/company/$id", CompanyResponse::class.java)

        assertThat(responseEntity.statusCode).isEqualTo(HttpStatus.OK)
        assertThat(responseEntity.body).usingRecursiveComparison()
                .ignoringFields("id")
                .isEqualTo(createCompanyResponse)

    }
}
package com.sedex.connect.company.controller

import com.fasterxml.jackson.databind.ObjectMapper
import com.sedex.connect.company.TestData.testCompanyData
import com.sedex.connect.company.TestData.testCompanyRequestData
import com.sedex.connect.company.TestData.testCompanyResponseData
import com.sedex.connect.company.controller.dto.CompanyList
import com.sedex.connect.company.exception.ResourceNotFountException
import com.sedex.connect.company.service.CompanyService
import org.junit.jupiter.api.Test
import org.mockito.Mockito.`when`
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest
import org.springframework.boot.test.mock.mockito.MockBean
import org.springframework.http.MediaType
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get
import org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post
import org.springframework.test.web.servlet.result.MockMvcResultHandlers
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.content
import org.springframework.test.web.servlet.result.MockMvcResultMatchers.status
import java.util.*

@WebMvcTest(CompanyController::class)
class CompanyControllerTest {

    private val objectMapper = ObjectMapper()

    @Autowired
    private lateinit var mockMvc: MockMvc

    @MockBean
    private lateinit var companyService: CompanyService

    @Test
    fun testPostCompany() {

        // Given
        val randomUUID = UUID.randomUUID()

        val company = testCompanyData(randomUUID)
        val createCompanyRequest = testCompanyRequestData()
        val createCompanyResponse = testCompanyResponseData(randomUUID.toString())

        //When
        `when`(companyService.createCompany(createCompanyRequest)).thenReturn(company)
        val writeValueAsString = objectMapper.writeValueAsString(createCompanyRequest)

        mockMvc.perform(post("/company").content(writeValueAsString)
                .contentType(MediaType.APPLICATION_JSON).accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(createCompanyResponse)))
    }

    @Test
    fun testGetCompanyList() {

        //Given
        val randomUUID = UUID.randomUUID()

        val company = testCompanyData(randomUUID)
        val createCompanyResponse = testCompanyResponseData(randomUUID.toString())

        //When
        `when`(companyService.getCompanyList()).thenReturn(listOf(company))

        mockMvc.perform(get("/company").accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(CompanyList(listOf(createCompanyResponse)))))

    }


    @Test
    fun testGetCompanyById() {
        //Given
        val randomUUID = UUID.randomUUID()
        val id = randomUUID.toString()

        val company = testCompanyData(randomUUID)
        val createCompanyResponse = testCompanyResponseData(id)

        //When
        `when`(companyService.getCompany(id)).thenReturn(company)

        mockMvc.perform(get("/company/$id").accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isOk())
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))
                .andExpect(content().json(objectMapper.writeValueAsString(createCompanyResponse)))

    }


    @Test
    fun testGetCompanyByIdNotFound() {
        //Given
        val randomUUID = UUID.randomUUID()
        val id = randomUUID.toString()

        //When
        `when`(companyService.getCompany(id)).thenThrow(ResourceNotFountException("ResourceNotFountException"))

        mockMvc.perform(get("/company/$id").accept(MediaType.APPLICATION_JSON))
                .andDo(MockMvcResultHandlers.print())
                .andExpect(status().isNotFound)
                .andExpect(content().contentType(MediaType.APPLICATION_JSON))

    }

}

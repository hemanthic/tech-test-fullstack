package com.sedex.connect.company

import com.sedex.connect.company.controller.dto.Address
import com.sedex.connect.company.controller.dto.CompanyRequest
import com.sedex.connect.company.controller.dto.CompanyResponse
import com.sedex.connect.company.repository.entity.Company
import com.sedex.connect.company.repository.entity.CompanyAddress
import java.util.*

object TestData {

    fun testCompanyResponseData(id: String): CompanyResponse {
        val address = Address("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val createCompanyResponse = CompanyResponse(id, "Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", address)
        return createCompanyResponse
    }

    fun testCompanyData(randomUUID: UUID?): Company {
        val companyAddress = CompanyAddress("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val company = Company("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", companyAddress)
        company.id = randomUUID
        return company
    }

    fun testCompanyRequestData(): CompanyRequest {
        val address = Address("5", "Old Bailey", "London", "Greater London",
                "EC4M 7BA", "UK")
        val createCompanyRequest = CompanyRequest("Sedex", "supply-chain",
                "Supply chain management", "01-01-2004", "helpdesk@sedex.com",
                "+44 (0)20 7902 2320", address)
        return createCompanyRequest
    }
}
# Company microservice

This repo is responsible for following tasks;
* Store company details
* Fetch company details

## How to run locally

### Prerequisite 
 Java 11 is installed and JAVA_HOME is set.

### App to run
Application uses embedded tomcat server and h2 database (persisted to file at user dir). Execute following command in terminal/bash

```./gradle clean bootRun```

Check instance is running healthy:
    ```http://localhost:8080/interview/v0/actuator/health```

### How to run unit tests locally

```./gradle clean test -i```

Swagger ui:
```http://localhost:8080/interview/v0/swagger-ui.html```


Scope of improvement for future:

* Proper database persistence integration
* BDD black box testing would be more preferred for acceptance testing.
* Please check, added Javadoc comments at necessary classes   
 
Please note: Basic project skeleton generated from Spring Initializr: https://start.spring.io/